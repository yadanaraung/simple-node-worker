const express = require("express");
const app = express();
const port = 3000;
let counter = 0;
const { Worker } = require("worker_threads");
app.get("/", (req, res) => {
  counter++;
  res.status(200).json({ counter: counter });
});

// method not using worker
app.get("/heavy", (req, res) => {
  let total = 0;
  for (i = 0; i < 2000000000; i++) {
    total++;
  }
  res.status(200).json({ total: total });
});

// using worker
app.get("/heavy-worker", (req, res) => {
  const worker = new Worker("./worker.js");
  worker.postMessage(2000000000);
  worker.on("message", (data) => {
    res.status(200).json({ total: data });
  });
  // can handle error and see error detail
  worker.on("error", (error) => {
    console.log("error=>");
    console.log(error);
  });
  // can handle error by checking code and also can know the worker is done.
  /* if code == 0 - the worker done successfully, else - the worker fail */
  worker.on("exit", (code) => {
    console.log("exit=>");
    console.log(code);
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
